# Jupyter Docker

_Set of useful Docker + Jupyter images with GPU support_
<br/>
<br/>

Setup
----

Recursive flag is required to include faiss submodule

```bash
$ git clone https://gitlab.com/bastiansg_bas/jupyter-docker --recursive
```

This repo requires Docker 20.10 or above

If you want to run the jupyter notebooks on a GPU you also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

Usage
-----

You can do _make build_, _make run_ and _make push_ with the required env **IMAGE_NAME**

### Examples

##### _jupyter-base_ build example
```bash
$ make build IMAGE_NAME=jupyter-base
```
##### _python-cuda_ run example
```bash
$ make run IMAGE_NAME=python-cuda
```
##### _python-faiss-cpu_ push example
```bash
$ make push IMAGE_NAME=python-faiss-cpu
```
